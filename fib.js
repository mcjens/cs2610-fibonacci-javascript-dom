// Set the document title
document.title = 'TODO: Dynamic Fibonacci Sequence in JavaScript';

var display = document.createElement('div');
display.setAttribute('class', 'flex');



// Create a red div in the body
var div = document.createElement('div');
div.setAttribute('class', 'red shadowed stuff-box');
document.querySelector('body').appendChild(div);


// Add instructions
var para = document.createElement('p');
para.textContent = "Fib()";
div.appendChild(para);


var slid = document.createElement('input');
slid.type = "range";
slid.max = "50";
slid.min = "0";
slid.value = "0";
slid.oninput = function(){
  para.textContent = "Fib(" + this.value + ")";
};
slid.onchange = function(){
  fibonacciCalc(this.value);
};
div.appendChild(slid);

var fibonacciCalc = function(position){
  while(display.firstChild){
    display.removeChild(display.firstChild);
  }
  var current = 0;
  var past = 0;
  if(position == 0){
    return 0;
  }
  for(let i=0; i < position; i++){
    current += past;
    if(i !== 0 && past === 0){
      current = 1;
    }
    past = current - past;
    var flexed = document.createElement('div');
    flexed.setAttribute('class', 'fib');
    flexed.innerHTML = current;
    display.appendChild(flexed);
  }
};

div.appendChild(display);
